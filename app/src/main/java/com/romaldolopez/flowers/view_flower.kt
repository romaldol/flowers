package com.romaldolopez.flowers

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

class view_flower : AppCompatActivity() {
    private var delete: MenuItem? = null
    private var logoutButton: MenuItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_flower)



        val imgView: ImageView
        val name_flower:TextView
        val type_flower:TextView

        val intent = intent
        val image : String = intent.getStringExtra("picture")
        val name : String = intent.getStringExtra("name")
        val type : String = intent.getStringExtra("type")

        imgView = findViewById(R.id.plantImage)
        name_flower = findViewById(R.id.name_Flower)
        type_flower = findViewById(R.id.type_Flower)


        Glide.with(this)
            .load(image)
            .into(imgView)

        name_flower.setText(name)
        type_flower.setText(type)




    }


}
