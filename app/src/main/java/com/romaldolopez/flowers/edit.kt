package com.romaldolopez.flowers

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_add_flower.*

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.cardview_item_flower.*
import kotlinx.android.synthetic.main.content_edit.*
import kotlinx.android.synthetic.main.view_flower.*
import java.util.*
import kotlin.collections.ArrayList
import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import java.nio.file.Files.delete
import android.view.View.OnAttachStateChangeListener







class edit : AppCompatActivity() {
    lateinit var key:String
    internal var storage: FirebaseStorage? = null
    internal var storageReference: StorageReference?=null
    val arrayList=ArrayList<flowerList>()
    var b: Boolean? = null
    private var filePath: Uri?= null
   // var progressDiolog = null

    //private val progressDialog = ProgressDialog(this)
    private lateinit var databaseRef: DatabaseReference
            override fun onCreate(savedInstanceState: Bundle?) {
                super.onCreate(savedInstanceState)
                setContentView(R.layout.content_edit)
                setSupportActionBar(toolbar)
                databaseRef = FirebaseDatabase.getInstance().reference
                key = databaseRef.push().key.toString()
                storage = FirebaseStorage.getInstance()
                storageReference = storage!!.reference
               // b=false
                val imgView: ImageView
                val name_flower: EditText
                val type_flower: EditText

                val intent = intent
                val image: String = intent.getStringExtra("picture")
                val name: String = intent.getStringExtra("name")
                val type: String = intent.getStringExtra("type")

                imgView = findViewById(R.id.displayImage2)
                name_flower = findViewById(R.id.nameEdit)
                type_flower = findViewById(R.id.typeEdit)


                Glide.with(this)
                    .load(image)
                    .into(imgView)

                name_flower.setText(name)
                type_flower.setText(type)

                val btn_click_me = findViewById(R.id.uploadtoFirebase) as Button

                btn_click_me.setOnClickListener {
                    if(b==true){

                        pushPic(name_flower,type_flower)
                        val intent = Intent(this, MainActivity::class.java)
                        // start your next activity
                        startActivity(intent)

                    }else{
                        updatePlant(image,name_flower.text.toString(),type_flower.text.toString())
                        val intent = Intent(this, MainActivity::class.java)
                        // start your next activity
                        startActivity(intent)


                    }
                }

            }
fun pushPic(name_flower:EditText, type_flower:EditText){
    val progressDialog = ProgressDialog(this)
    progressDialog.setTitle("uploading...")
    progressDialog.show()
    val imageRef = storageReference!!.child("images/"+ UUID.randomUUID().toString())
    var uploadTask= imageRef.putFile(filePath!!)
    uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>{ task ->

        if (!task.isSuccessful){
            task.exception?.let {
                throw it
            }
        }
        return@Continuation imageRef.downloadUrl
    }).addOnCompleteListener { task ->
        var strParentKey: String
        if(task.isSuccessful){
            val downloadUri = task.result

            //writeNewFlower(key, downloadUri.toString(),editText_Name.text.toString(),editText_Type.text.toString())

            updatePlant(downloadUri.toString(),name_flower.text.toString(),type_flower.text.toString())

        }else{

        }
    }
}




fun updatePlant(image:String, name:String, type:String){
   // val position: Int = intent.getIntExtra("index", 0)
    val position2: String = intent.getStringExtra("index")
   // print("position: $position2")

    val user = flowerList(image,name,type)

    val database2 = databaseRef.child("flower1")

  // databaseRef.child("flower1").child(key).setValue(user)


    val menuListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {

          for (child in dataSnapshot.children) {
               if(child.key.equals(position2)) {
                   child.ref.child("image").setValue(image)
                   child.ref.child("name").setValue(name)
                   child.ref.child("type").setValue(type)
                   //progressDialog.dismiss()


               }
           }




        }

        override fun onCancelled(databaseError: DatabaseError) {
            println("loadPost:onCancelled ${databaseError.toException()}")
        }
    }

    database2.addListenerForSingleValueEvent(menuListener)

}



    fun uploadPicture2(view: View) {
        // Do something in response to button
        //check runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE);
            }
            else{
                //permission already granted
                pickImageFromGallery();

            }
        }
        else{
            //system OS is < Marshmallow
            pickImageFromGallery();


        }
    }
    private fun pickImageFromGallery() {

        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    companion object {
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted

                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        b=true

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            filePath = data?.data
            Glide.with(this)
                .load(data?.data)
                .into(displayImage2)

        }
    }

}
