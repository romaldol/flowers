package com.romaldolopez.flowers

import android.content.Intent
import android.nfc.Tag
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class loginpage : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    var firebaseAuth: FirebaseAuth? = null
    var mAuthListener: FirebaseAuth.AuthStateListener? = null

    private var etEmail: EditText? = null
    private var etPassword: EditText? = null

    //private var fab: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loginpage)


        // get reference to all views
        var btn_submit = findViewById(R.id.btn_submit) as Button
         // ...
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        //fab =  findViewById<FloatingActionButton>(R.id.fab)

        etEmail = findViewById<View>(R.id.et_email) as EditText
        etPassword = findViewById<View>(R.id.et_password) as EditText


        // set on-click listener
        btn_submit.setOnClickListener {

            val email = etEmail?.text.toString()
            val password = etPassword?.text.toString()
            auth!!.signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Toast.makeText(baseContext, "signInWithCustomToken:success", Toast.LENGTH_SHORT).show()
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()

                    }
                }


            val intent = Intent(this, MainActivity::class.java)
            // start your next activity
            startActivity(intent)

            Toast.makeText(this@loginpage, email, Toast.LENGTH_LONG).show()

            // your code to validate the user_name and password combination
            // and verify the same

        }

    }


    override fun onResume() {
        super.onResume()
        val currentUser = auth.currentUser


    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser


    }



}
