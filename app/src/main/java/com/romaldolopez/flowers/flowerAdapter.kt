package com.romaldolopez.flowers

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.media.Image
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.cardview_item_flower.view.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.storage.UploadTask
import java.util.*
import kotlin.collections.ArrayList


class flowerAdapter(val context:Context, val arrayList: ArrayList<flowerList>) : RecyclerView.Adapter<flowerAdapter.ViewHolder>() {
    private lateinit var databaseRef: DatabaseReference
    lateinit var key:String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.cardview_item_flower, parent, false))

   }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.flower_type?.text= arrayList.get(position).type

        Glide.with(context)
            .load(arrayList.get(position).image)
            .into(holder.imgView)
        holder.itemView.setOnClickListener {

            val intent = Intent(context, view_flower::class.java)
            intent.putExtra("picture", arrayList.get(position).image)
            intent.putExtra("name", arrayList.get(position).name)
            intent.putExtra("type", arrayList.get(position).type)
            context.startActivity(intent)
        }
        holder.itemView.setOnLongClickListener {
            val dialogBuilder = AlertDialog.Builder(context)
            dialogBuilder.setMessage("testing")
                .setCancelable(false)
            val builder = AlertDialog.Builder(context)
            // Set the alert dialog title
            builder.setTitle("Delete")

            // Display a message on alert dialog
            builder.setMessage("Are you sure you want to delete?")
            databaseRef = FirebaseDatabase.getInstance().reference


            builder.setPositiveButton("YES"){dialog, which ->
              removeItem(position)

            }

            // Display a negative button on alert dialog
            builder.setNegativeButton("Edit"){dialog,which ->
                Toast.makeText(context,"You are not agree.",Toast.LENGTH_SHORT).show()
            editItem(position)

            }


            // Display a neutral button on alert dialog
            builder.setNeutralButton("Cancel"){_,_ ->
                Toast.makeText(context,"You cancelled the dialog.",Toast.LENGTH_SHORT).show()
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()


            dialog.show()

            return@setOnLongClickListener true
        }


    }

    override fun getItemCount(): Int {
        return arrayList.size

    }



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var imgView:ImageView

        init {
            imgView = itemView.imageView
        }

    }

    fun editItem(position: Int){
        val intent = Intent(context, edit::class.java)
        intent.putExtra("index", arrayList.get(position).uuid)
        intent.putExtra("picture", arrayList.get(position).image)
        intent.putExtra("name", arrayList.get(position).name)
        intent.putExtra("type", arrayList.get(position).type)
        context.startActivity(intent)

    }


    fun removeItem(position: Int) {
        val database2 = databaseRef.child("flower1")

        val menuListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (child in dataSnapshot.children) {


                    if(child.key.equals(arrayList.get(position).uuid)) {

                        child.ref.removeValue()
                        //database2.child(child.key.toString()).removeValue()


                        notifyDataSetChanged()
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }

        database2.addListenerForSingleValueEvent(menuListener)
    }

}
