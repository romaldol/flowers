package com.romaldolopez.flowers

import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.cardview_item_flower.*
import android.widget.Toast
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.AuthResult
import com.google.android.gms.tasks.Task
import android.support.annotation.NonNull
import android.support.design.widget.Snackbar
import com.google.android.gms.tasks.OnCompleteListener



class MainActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    val keys = ArrayList<String>()//Creating an empty arraylist
    val posts = ArrayList<flowerList>()//Creating an empty arraylist
    //val posts: List<flowerList> = ArrayList<flowerList>()
    //lateinit var posts:MutableList<flowerList>
    lateinit var recyclerView: RecyclerView
    private var loginButton: MenuItem? = null
    private var logoutButton: MenuItem? = null

    private val showLogin:MenuItem?=null
    private var showLogout:Boolean?=null
    private lateinit var auth: FirebaseAuth

    var context=this
    var connectivity : ConnectivityManager? = null
    var info : NetworkInfo?=null
    private var fab: FloatingActionButton? = null
    private val mCustomToken: String? = null

    private var etEmail: EditText? = null
    private var etPassword: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        database = FirebaseDatabase.getInstance().reference
        auth = FirebaseAuth.getInstance()

        fab =  findViewById<FloatingActionButton>(R.id.fab)

       items()

        auth.addAuthStateListener {
            if (auth.getCurrentUser() != null) {
                // already signed in
                fab?.show()

                fab?.setOnClickListener { view ->
                    val intent = Intent(this, addFlower::class.java)
                    // start your next activity
                    startActivity(intent)
                }





            }else {

                fab?.hide()
            }
        }




    }

    fun items(){
        if(isConnected()){
            getItems()

            val layout = findViewById<RelativeLayout>(R.id.layout)
            layout.setVisibility(View.INVISIBLE);


        }else{
            // setContentView(R.layout.nointernet)
            val imageView = ImageView(this)

            // accessing our relative layout from activity_main.xml
            val layout = findViewById<RelativeLayout>(R.id.layout)

            // Add ImageView to LinearLayout
            layout?.addView(imageView)


            val fab = findViewById<FloatingActionButton>(R.id.fab)
            fab.hide()

        }


    }
    fun getItems(){
        recyclerView = findViewById(R.id.recyclerview_id)
        val postListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                keys.add(dataSnapshot.key.toString())
                if(dataSnapshot.exists()){
                    posts.clear()
                    for (e in dataSnapshot.children) {
                        val post = e.getValue(flowerList::class.java)
                        // ...
                        posts.add(post!!)
                    }
                    recyclerView.setLayoutManager(GridLayoutManager(applicationContext, 3))

                    val adapter = flowerAdapter(this@MainActivity, posts)
                    recyclerView.adapter = adapter
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                println("loadPost:onCancelled ${databaseError.toException()}")                // ...
            }
        }
        database.child("flower1").addValueEventListener(postListener)




    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        loginButton = menu.findItem(R.id.action_login);
        logoutButton = menu.findItem(R.id.action_logout);


        return true;
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {


        if (auth.getCurrentUser() != null) {
            // already signed in
            logoutButton?.isVisible = true
            loginButton?.isVisible=false

        }else {
            logoutButton?.isVisible=false
            loginButton?.isVisible = true
            fab?.hide()
        }




        return super.onPrepareOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return when (item.itemId) {
            R.id.action_login-> {
                val intent = Intent(this, loginpage::class.java)
                // start your next activity
                startActivity(intent)
                true
            }
            R.id.action_logout-> {
                    FirebaseAuth.getInstance().signOut()

                val fab = findViewById<FloatingActionButton>(R.id.fab)
                fab.hide()
                    Toast.makeText(baseContext, "Signout.",
                        Toast.LENGTH_SHORT).show()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*fun isOnline(context: Context) {
        val imgView: ImageView
        //imgView = findViewById(R.id.noWifi)

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo

        if (networkInfo != null && networkInfo.isConnected) {
            getItems()
            Toast.makeText(this, "Your connected", Toast.LENGTH_SHORT).show()

        }else{
            Toast.makeText(this, "Your not connected", Toast.LENGTH_SHORT).show()


        }

    } */

    /*fun getImage(imageName: String): Int {

        return resources.getIdentifier(imageName, "drawable", packageName)
    } */

    fun isConnected(): Boolean
    {
        connectivity = context.getSystemService(Service.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null){
            info=connectivity!!.activeNetworkInfo
            if(info != null){
                if(info !!.state == NetworkInfo.State.CONNECTED ){
                    return true
                }
            }
        }
        return false

    }
}



