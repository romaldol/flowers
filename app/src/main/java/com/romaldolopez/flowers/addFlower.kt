package com.romaldolopez.flowers

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_flower.*
import android.net.Uri
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.EditText
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.content_main.*
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import com.bumptech.glide.Glide as Glide1
import com.google.firebase.database.DataSnapshot



class addFlower : AppCompatActivity() {
    private lateinit var database: DatabaseReference

    private var filePath: Uri ?= null
   lateinit var key:String
    internal var storage: FirebaseStorage? = null
    internal var storageReference:StorageReference?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_flower)

        database = FirebaseDatabase.getInstance().reference

        storage = FirebaseStorage.getInstance()
        storageReference = storage!!.reference
        key = database.push().key.toString()

    }

    fun uploadToFirebase(view: View){
        val editText_Name = findViewById(R.id.nameEdit) as EditText
        val editText_Type = findViewById(R.id.typeEdit) as EditText

        val imageRef = storageReference!!.child("images/"+ UUID.randomUUID().toString())
        var uploadTask= imageRef.putFile(filePath!!)


        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>{ task ->
            if (!task.isSuccessful){
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation imageRef.downloadUrl
        }).addOnCompleteListener { task ->
            var strParentKey: String
            if(task.isSuccessful){
                val downloadUri = task.result

                writeNewFlower(key, downloadUri.toString(),editText_Name.text.toString(),editText_Type.text.toString())


            }else{

            }
        }



    }

    private fun writeNewFlower(id:String, picture: String, name: String, type: String) {


        //val lists: ArrayList<flowerList> = ArrayList<flowerList>()
       //lists.add(flowerList(picture,name, type))//Adding object in arraylist

        val user = flowerList(id, picture,name,type)

        println("this is the list: " + user)

        if(filePath != null){
            val progressDiolog = ProgressDialog(this)
            progressDiolog.setTitle("uploading...")
            progressDiolog.show()
            //key = database.push().key.toString()

            database.child("flower1").child(key).setValue(user)
                .addOnSuccessListener {
                    progressDiolog.dismiss()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                    Toast.makeText(applicationContext, "File Uploaded.", Toast.LENGTH_SHORT).show()

                }
                .addOnFailureListener {
                    progressDiolog.dismiss()
                    Toast.makeText(applicationContext, "File Failed to upload.", Toast.LENGTH_SHORT).show()
                }


        }

        //recyclerview_id.layoutManager = GridLayoutManager(this,2)
       // recyclerview_id.adapter = flowerAdapter(this, list)
    }
    fun uploadPicture(view: View) {
        // Do something in response to button
        //check runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE);
            }
            else{
                //permission already granted
                pickImageFromGallery();
            }
        }
        else{
            //system OS is < Marshmallow
            pickImageFromGallery();
        }
    }
    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    companion object {
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {


        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            filePath = data?.data
            Glide1.with(this)
                    .load(data?.data)
                    .into(displayImage)

        }
    }




}


